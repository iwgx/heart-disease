import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"]
})
export class AppComponent implements OnInit {
  predictionResult = "NA";

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eigthFormGroup: FormGroup;
  ninthFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  twelvthFormGroup: FormGroup;
  thirteenthFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      age: [null, [Validators.required, Validators.min(0)]]
    });
    this.secondFormGroup = this._formBuilder.group({
      sex: [null, [Validators.required, Validators.min(0), Validators.max(1)]]
    });
    this.thirdFormGroup = this._formBuilder.group({
      chest_pain: [
        null,
        [Validators.required, Validators.min(1), Validators.max(4)]
      ]
    });
    this.fourthFormGroup = this._formBuilder.group({
      resting_blood: [null, Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      serum_cholesterol: [null, Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      fasting_blood: [
        null,
        [Validators.required, Validators.min(0), Validators.max(1)]
      ]
    });
    this.seventhFormGroup = this._formBuilder.group({
      resting_ecg: [
        null,
        [Validators.required, Validators.min(0), Validators.max(2)]
      ]
    });
    this.eigthFormGroup = this._formBuilder.group({
      maxheart_rate: [null, Validators.required]
    });
    this.ninthFormGroup = this._formBuilder.group({
      exercise_induced: [
        null,
        [Validators.required, Validators.min(0), Validators.max(1)]
      ]
    });
    this.tenthFormGroup = this._formBuilder.group({
      st_depression: [null, Validators.required]
    });
    this.eleventhFormGroup = this._formBuilder.group({
      peak_exercise: [
        null,
        [Validators.required, Validators.min(1), Validators.max(3)]
      ]
    });
    this.twelvthFormGroup = this._formBuilder.group({
      number_of: [
        null,
        [Validators.required, Validators.min(0), Validators.max(3)]
      ]
    });
    this.thirteenthFormGroup = this._formBuilder.group({
      thal: [null, [Validators.required, Validators.pattern(/3|6|7/)]]
    });
  }

  resetResult() {
    this.predictionResult = "NA";
  }

  onFormSubmit() {
    const mergedValue = [];

    mergedValue.push(this.firstFormGroup.value.age);
    mergedValue.push(this.secondFormGroup.value.sex);
    mergedValue.push(this.thirdFormGroup.value.chest_pain);
    mergedValue.push(this.fourthFormGroup.value.resting_blood);
    mergedValue.push(this.fifthFormGroup.value.serum_cholesterol);
    mergedValue.push(this.sixthFormGroup.value.fasting_blood);
    mergedValue.push(this.seventhFormGroup.value.resting_ecg);
    mergedValue.push(this.eigthFormGroup.value.maxheart_rate);
    mergedValue.push(this.ninthFormGroup.value.exercise_induced);
    mergedValue.push(this.tenthFormGroup.value.st_depression);
    mergedValue.push(this.eleventhFormGroup.value.peak_exercise);
    mergedValue.push(this.twelvthFormGroup.value.number_of);
    mergedValue.push(this.thirteenthFormGroup.value.thal);

    this.http
      .post("http://localhost:5000/analyze", mergedValue)
      .subscribe(
        (result: number) => { this.predictionResult = result.toString() }, 
        (err: Error) => { this.predictionResult = 'Fail' }
      );
  }
}
